function executedTime(duration) {
  
   const seconds = Math.floor((duration / 1000) % 60);
   const minutes = Math.floor((duration / (1000 * 60)) % 60);
   const hours = Math.floor((duration / (1000 * 60 * 60)) % 24);
   const days = Math.floor((duration / (1000 * 60 * 60 * 24)) % 365);

  return ` ${days} Days -  ${hours} hours -  ${minutes} minutes -  ${seconds} seconds`;
}
executedTime();


function time() {
  const firstTime = new Date();
  const interval = setInterval(() => {
    const normalTime = new Date();
    const time = normalTime - firstTime;
    console.log(executedTime(time));
  }, 5000);
}

time();
