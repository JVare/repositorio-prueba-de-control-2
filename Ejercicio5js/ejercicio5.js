// /* https://rickandmortyapi.com/api/episode */
// https://rickandmortyapi.com/api/character/

async function getCharacters() {
  try {
    const response = await fetch("https://rickandmortyapi.com/api/episode");
    const data = await response.json();
    console.log(data);
 
      const allEpisodes = data.results;
      
      const januaryEpisodes = allEpisodes.filter((episode)=>{ return episode.air_date.includes('January')})
      console.log(januaryEpisodes);
      
      const charactersJanuary = januaryEpisodes.map((episode)=>{
        return episode.characters;
        
      });
      console.log(charactersJanuary);

    
  } catch (error) {
    console.log(error);
  }
};

getCharacters();

async function characterName () {
  try {
    const response = await fetch('https://rickandmortyapi.com/api/character');
    const data = await response.json();
    console.log(data);
 
  } catch (error) {
    console.log(error);
  }
}

characterName();