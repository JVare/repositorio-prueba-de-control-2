

Number.prototype.bin2dec = function () {
  var sBinary = this.toString(10);
  for (var i = 0; i < sBinary.length; i++) {
    if (sBinary.charAt(i) != "0" && sBinary.charAt(i) != "1") return false;
  }
  var bin = sBinary;
  var dec = 0;
  var binl = bin.length;
  var p = 0;
  var n = 0;
  while (binl > 0) {
    p = bin.length - binl;
    binl--;
    n = parseInt(bin.substr(binl, 1));
    dec += n * Math.pow(2, p);
  }
  return Number(dec);
};

var num = 1001010;
alert(num.bin2dec());
// 25
