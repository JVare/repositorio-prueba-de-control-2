let pIni = document.querySelectorAll("p").item(0);
let pFnl = pIni.innerHTML;
let pArray = pFnl.split(/[\s,\.,\"]+/);

pArray.forEach((word) => {
  if (word.length > 5) {
    pFnl = pFnl.split(word).join(`<u>${word}</u>`);
  }
});

pIni.innerHTML = pFnl;

let p = document.getElementsByTagName("p").item(1);
let p_1 = p.innerHTML;
let p_2 = p_1.split(/[\s,\.,\"]+/);

p_2.forEach((wrd) => {
  if (wrd.length > 5) {
    p_1 = p_1.split(wrd).join(`<u>${wrd}</u>`);
  }
});

p.innerHTML = p_1;

let pBeg = document.getElementsByTagName("p").item(2);
let pMid = pBeg.innerHTML;
let pFn = pMid.split(/[\s,\.,\"]+/);

pFn.forEach((wrd) => {
  if (wrd.length > 5) {
    pMid = pMid.split(wrd).join(`<u>${wrd}</u>`);
  }
});

pBeg.innerHTML = pMid;
