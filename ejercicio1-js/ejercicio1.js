// https://randomuser.me/api/
const URL = "https://randomuser.me/api/";

function validateUser(usersNumber) {
  if (Number.isNaN(Number(usersNumber))) {
    throw new Error(`No es un número`);
  }
}
const getUsers = async (usersNumber) => {
  try {
    const response = await fetch(
      `https://randomuser.me/api/?results=${usersNumber}`
    );

    const { results } = await response.json();
    const users = results;

    const usersResult = users.map((user) => ({
      picture: user.picture.large,
      firstName: user.name.first,
      lastName: user.name.last,
      country: user.location.state,
      email: user.email,
      gender: user.gender,
      username: user.login.username,
    }));
    return usersResult;
  } catch (error) {
    console.log(error);
  }
};

const resultUsers = async () => {
  const users = await getUsers(5);
  console.log(users);
};

resultUsers();
